<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'int_conectics');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>SS}d3fnF42*aw% @ Lw7IC_&e vf@Th%Iwp.I9%R&I8GeUV8Rb@+;*_y#53E$m)');
define('SECURE_AUTH_KEY',  '.TaV$Ju2CtxC6~*#P|:3c.^R%M]wOmzU$lW*GzRJs^[_UjS!E+(1D1o9UQVz%/Z:');
define('LOGGED_IN_KEY',    '0hL$oo7,MU&gUp/8( nM647#bhlukfz%aH)8P:fk&s(-/0Pa!ty9f&ru3^*?@<3,');
define('NONCE_KEY',        '5hc_tK3dA$EPQJxb$2}FP P)^VqewSWX):S^@2!r!CoS2`n[,d78uRBX Ox@O&C,');
define('AUTH_SALT',        ':QBQ<{|WbJF906O.NTI$VZWYrfks8Bc9t=3:IoM}0)M9U)`HCK@ 7H$wM^yvGhfQ');
define('SECURE_AUTH_SALT', 'M/?XNy>TE0:P{2BLD;_mdghR_4ceY@Ln;[cGK({7Ae[,q:q)quH@O*3-j=CUYQd ');
define('LOGGED_IN_SALT',   ' 9i[E6N-J*<Srx)Q@_jb5nk0;s~oLm#?c8.~Rp^RI: +q?v=j/`!)7qCk7;*-rAA');
define('NONCE_SALT',       '}<[_!q5Ii{|e/=/)m#4cyK=w7`w9Sbvfd+ukPG&y=HM+]D|NRC+U~kzT:%2[KH36');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
