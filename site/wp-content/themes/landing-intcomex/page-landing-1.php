<?php get_header("landing");?>
    <div class="slider">
        <?php echo do_shortcode('[rev_slider alias="main_slider"]'); ?>
    </div>
    <div class="clear"></div>
    <div class="container">
        <div class="form-box col-xs-12 col-sm-12 col-md-offset-2 col-md-8 text-center">
            <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]');?>
        </div>
    </div>
    <div class="pre-footer">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-6">
                <div class="row">
                    <p>Tómate un café con nosotros en <strong>CONECT-CAFÉ</strong> y conoce más acerca de estas soluciones.</p>
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/tablet.png">
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <p class="bot-text"><strong>Para ganar la tablet: </strong>La tablet será rifada entre los asistentes a CONECT-CAFÉ que hicieron uso de este demo.</p>
    </div>
<?php get_footer();?>