<?php get_header("landing");?>
    <div class="slider">
        <?php echo do_shortcode('[rev_slider alias="slider-landing-2"]'); ?>
    </div>
    <div class="clear"></div>
    <div class="container">
        <div class="form-box col-xs-12 col-sm-12 col-md-offset-2 col-md-8 text-center">
            <?php echo do_shortcode('[contact-form-7 id="12" title="Contact form landing 2"]');?>
        </div>
    </div>
<?php get_footer();?>