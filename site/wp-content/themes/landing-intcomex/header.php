<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--FAVICON-->
    <link rel="icon" href="<?php echo theme_get_option( 'favicon_image' ); ?>" type="image/x-icon">
    <!-- APPLE TOUCH ICON-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_get_option( 'apple_icon_57' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_get_option( 'apple_icon_72' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_get_option( 'apple_icon_114' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_get_option( 'apple_icon_144' ); ?>">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600,800,300italic' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="general-wrapper">
        <header>
            <nav class="navbar navbar-conect-ics navbar-fixed-top">
                <div class="container">
                    <div class="row">
                        <div class="navbar-header">
                            <a href="<?php echo get_option('home'); ?>" >
                                <img src="<?php echo theme_get_option( 'logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>" />
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-7">
                            <div id="navbar" class="navbar-collapse collapse">
                                <?php wp_nav_menu(array('menu_class'=> 'nav navbar-nav main-menu','theme_location' => 'main-menu', 'container' => false));?>
                            </div><!--/.nav-collapse -->
                        </div>
                        <div class="aux-logo pull-right">
                            <img src="<?php echo bloginfo('template_url');?>/assets/img/microsoft.jpg" alt="Microsoft" title="Microsoft"/>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <div class="clear"></div>