<?php
// Initialize the metabox class
if ( file_exists( dirname( __FILE__ ) . '/lib/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/lib/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/lib/CMB2/init.php';
}
/* OPCIONES DEL TEMA */
require_once ( get_template_directory() . '/admin/theme-options.php' );

/* SVG */
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/* Reference Styles and JS */
add_filter( 'wp_enqueue_scripts', 'wpb_adding_scripts', 0 );
function themeslug_enqueue_style() {
	wp_enqueue_style( 'libcss', get_stylesheet_directory_uri() .'/dist/lib.css', false ); 
    wp_enqueue_style( 'main', get_stylesheet_directory_uri() .'/css/styles.css', false ); 
    wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false ); 
}
function wpb_adding_scripts() {
    wp_enqueue_script( 'script-lib',get_stylesheet_directory_uri() . '/dist/lib.js','','',true);
    wp_enqueue_script( 'script-modernizr',get_stylesheet_directory_uri() . '/dist/modernizr-2.8.3-respond-1.4.2.min.js','','',true);
    if (is_home()){
        wp_enqueue_script( 'script-waypoints',get_stylesheet_directory_uri() . '/dist/jquery.waypoints.min.js','','',true);
        wp_enqueue_script( 'script-main',get_stylesheet_directory_uri() . '/js/script.js','','',true);
        wp_enqueue_script( 'google-maps','https://maps.googleapis.com/maps/api/js?key=AIzaSyBnSlVgBgZQLHKh_Czm0zf33OZFiErVY_8','','',true);
        wp_enqueue_script( 'lib-mapinfobox-js', get_stylesheet_directory_uri() . '/dist/infobox.js','','',true );
        wp_enqueue_script( 'script-map',get_stylesheet_directory_uri() . '/js/map.js','','',true);
        // data to maps the admin
        $latitud_map    = theme_get_option( 'latitud' );
        $longitud_map   = theme_get_option( 'longitud' );
        $name_site      = get_bloginfo ("name");
        $direccion      = theme_get_option( 'direccion' );
        $data_map       = array(
            'nameSite'      => $name_site,
            'latitudMap'    => $latitud_map,
            'longitudMap'   => $longitud_map,
            'infoBox'       => $direccion
        );
        wp_localize_script( 'script-map', 'object_map', $data_map );
    }
}
add_action( 'wp_enqueue_scripts', 'themeslug_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );  

/*Custom logo in login */
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo theme_get_option( 'logo' ); ?>);
            padding-bottom: 0px;
            width: 100%;
            height: 150px;
            -webkit-background-size: initial;
            -moz-background-size: initial;
            -o-background-size: initial;
            background-size: initial;
        }
    </style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo_url_title() {
    return 'Milo';
}
/* SOPORTE PARA IMAGENES EN EL POST Y REMOVER LOS ATRIBUTOS WIDHT & HEIGHT*/
if (function_exists('add_theme_support')) { 
    add_theme_support('post-thumbnails');
}
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
/* Registro Menus */
function register_my_menus() {
  register_nav_menus(
    array(
        'main-menu'     => 'Main Menu',
        //'menu-footer'     => 'Menu Footer'
    )
  );
}
add_action( 'init', 'register_my_menus' );

/**
    Excerpt Page 
**/
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}
/**
            PAGINADOR
*/
/* Paginador */

function base_pagination() {
    global $wp_query;

    $big = 999999999; // This needs to be an unlikely integer

    // For more options and info view the docs for paginate_links()
    // http://codex.wordpress.org/Function_Reference/paginate_links
    $paginate_links = paginate_links( array(
        'base'          => str_replace( $big, '%#%', get_pagenum_link($big) ),
        'current'       => max( 1, get_query_var('paged') ),
        'total'         => $wp_query->max_num_pages,
        'mid_size'      => 5,
        'prev_next'     => True,
        'prev_text'     => __('&nbsp;'),
        'next_text'     => __('&nbsp;'),
        'type'          => 'list'
    ) );

    // Display the pagination if more than one page is found
    if ( $paginate_links ) {
        echo '<div class="clear"></div><div id="paginador">';
        echo $paginate_links;
        echo '</div><!--// end .pagination -->';
    }
}

/* Paginador WP_query */
function pagination_query($query = null) {
    if ( !$query ) {
        global $wp_query;
        $query = $wp_query;
    }
     
    $big = 999999999; // need an unlikely integer
 
    $pagination = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total' => $query->max_num_pages,
        'mid_size' => 5,
        'prev_next'    => True,
        'prev_text'    => __('&nbsp;'),
        'next_text'    => __('&nbsp;'),
        'type'         => 'list'
            ) );
    ?>
    
    <div class="clear"></div>
    <div id="paginador">
            <?php echo $pagination; ?>
    </div> 
    <?php
}

/*Sidebar*/
add_action( 'widgets_init', 'sidebar_widget' );
function sidebar_widget() {
    register_sidebar( array(
        'name'          => __( 'Filter Category', THEME_LOCALE ),
        'id'            => 'filter-category',
    ));
}