<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--FAVICON-->
    <link rel="icon" href="<?php echo theme_get_option( 'favicon_image' ); ?>" type="image/x-icon">
    <!-- APPLE TOUCH ICON-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_get_option( 'apple_icon_57' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_get_option( 'apple_icon_72' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_get_option( 'apple_icon_114' ); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_get_option( 'apple_icon_144' ); ?>">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600,800,300italic' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="general-wrapper-landing">
    <header>
        <a href="<?php echo get_option('home'); ?>" >
            <img class="img-responsive" src="<?php echo theme_get_option( 'logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>" />
        </a>      
    </header>
    <div class="clear"></div>