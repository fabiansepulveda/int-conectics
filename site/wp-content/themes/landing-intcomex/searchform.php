<?php
$search_query = get_search_query();
if($search_query == "")
	$search_query = "";
?>

<form role="search" method="get" id="searchform" class="search-box-form" action="<?php echo home_url( '/' ); ?>">
    <input class="search-imput" placeholder="Search..." type="text" value="<?php echo $search_query; ?>" name="s" id="s" onblur="if (this.value == '') {this.value = '';}" onfocus="if (this.value == '') {this.value = '';}" />
</form>