var Lat = Number(object_map.latitudMap);
var Lng = Number(object_map.longitudMap);

var myLatLng = {lat: Lat, lng: Lng};

function initMap() {
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 11,
    center: myLatLng,
    draggable: true,
  	zoomControl: true, 
  	scrollwheel: true, 
  	disableDoubleClickZoom: true,
    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative.province","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.locality","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#abd8ea"},{"visibility":"on"}]}]
  });

  /*function initMarkers(map, markerData) {
    var newMarkers = [],
    marker;
    for(var i=0; i<markerData.length; i++) {
      var contentString = '<div class="content-infobox"><p>'+
      markerData[i].content +
      '</p></div>';
      marker = new google.maps.Marker({
        position:   markerData[i].latLng,
        map:        map,
        icon:       image,
        title:      object_map.nameSite
      }),
      infoboxOptions = {
        content: contentString,
        boxClass : "infoBox",
        alignBottom: true,
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-100,-30),
        zIndex: null,
        closeBoxMargin: "0px",
        closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1)
      };
      newMarkers.push(marker);
      newMarkers[i].infobox = new InfoBox(infoboxOptions);
      newMarkers[i].infobox.open(map, marker);
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              newMarkers[i].infobox.open(map, this);
              map.panTo(markerData[i].latLng);
          }
      })(marker, i));
    }
    return newMarkers;
  }*/

  var contentString = '<div class="content-infobox"><p>'+
      object_map.infoBox +
      '</p></div>';

  var infowindow = new InfoBox({
    content: contentString,
    boxClass : "infoBox",
    alignBottom: true,
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-100,-30),
    zIndex: null,
    closeBoxMargin: "0px",
    closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
    infoBoxClearance: new google.maps.Size(1, 1)
  });

  var image = {
    url: '../site/wp-content/themes/landing-intcomex/assets/img/icon-map.png',
    size: new google.maps.Size(14, 20),
    origin: new google.maps.Point(0,0),
    anchor: new google.maps.Point(0, 20)
  };
  
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: image,
    title: object_map.nameSite
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
  infowindow.open(map,marker);
  
  /*markers = initMarkers(map, [
        { latLng: new google.maps.LatLng(Lat, Lng), content: object_map.infoBox }
    ]);*/
}

google.maps.event.addDomListener(window, 'load', initMap);
google.maps.event.addDomListener(window, "resize", initMap);