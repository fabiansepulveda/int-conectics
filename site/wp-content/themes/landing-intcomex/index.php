<?php get_header(); ?>
<div class="wrapper-content">
    <section class="home" id="inicio">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">
                    <div class="box col-xs-12 col-sm-6 text-center">
                        <h2>Visión</h2>
                        <p>Nace Conect-ics como una empresa puertorriqueña con una visión regional clara de: "Empoderar los negocios de nuestros clientes para estar siempre productivos y siempre conectados”</p>
                    </div>
                    <div class="box col-xs-12 col-sm-6 text-center">
                        <h2>Misión</h2>
                        <p>La misión de Conect-ics se centra en "Apoyar a nuestros clientes en la toma de sus decisiones de negocios, basados en la selección de la mejor tecnología”</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clear"></div>
    <section class="solutions" id="soluciones">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">
                <div class="row">
                    <div class="title text-center">
                        <h3>En <strong>Comnect-ICS</strong> ofrecemos soluciones integrales y asesoría en:</h3>
                    </div>
                    <div class="box-left col-xs-12 col-sm-6">
                        <div class="circle">
                            <h5>Soluciones <strong>Estratégicas de Negocios</strong></h5>
                        </div>
                        <ul>
                            <li>Servicios de Consultoría estratégica, planificación y facilitación Tecnológica</li>
                            <li>Planificación de Negocios y Procesos.</li>
                            <li>Metodología de Solution Sales 360 y “Customer Centric Marketing”</li>
                            <li>Desarrollo de modelos de negocio de cloud y tecnología</li>
                        </ul>
                    </div>
                    <div class="box-right col-xs-12 col-sm-6">
                        <div class="circle">
                            <h5><strong>Servicios  y Tecnologías </strong>para el Cloud</h5>
                        </div>
                        <ul>
                            <li>Proyectos de Tecnología con Microsoft, Office 365 y Windows Azure</li>
                            <li>Proyectos de implementación de Microsoft Dynamics CRM.</li>
                            <li>Asesoría para implementación de servicios de Telecomunicaciones, VOIP e Internet.</li>
                            <li>Servicios de infraestructura de informática</li>
                            <li>Servicios de Operación y manejo de tecnologías informáticas.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clear"></div>
    <section class="brands text-center" id="marcas">
        <h3>A quienes representamos</h3>
        <div class="container">
            <div class="list">
                <div>
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/site/microsoft.jpg" />
                </div>
                <div>
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/site/onnet.png" />
                </div>
                <div>
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/site/triserve.png" />
                </div>
                <div>
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/site/nutanix.png" />
                </div>
                <div>
                    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/site/intcomex.png" />
                </div>
            </div>
        </div>
    </section>
    <div class="clear"></div>
    <section class="clients" id="clientes">
        <div class="container">
            <h3>Nuestros Clientes</h3>
            <ul>
                <li><span><strong>Banca</strong><br><p> - Bancos de Inversión y Offshore</p></span></li>
                <li><span><strong>Sector Salud</strong></span></li>
                <li><span><strong>Servicios Profesioanles</strong><br><p>- Bufetes de Abogados<br>
                    - Partners de Tecnología</p></span></li>
                <li><span><strong>Empresas de Telecomunicaciones y Datacenter Services</strong></span></li>
                <li><span><strong>Gobierno Federal de los Estados Unidos</strong></span></li>
            </ul>
        </div>
    </section>
    <div class="clear"></div>
    <section class="contact" id="contactenos">
        <div class="map" id="map-canvas"></div>
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-sm-offset-6 col-md-offset-8 col-md-4 form">
                <h3>Contáctenos</h3>
                <?php echo do_shortcode('[contact-form-7 id="25" title="Contact site" html_class="use-floating-validation-tip"]'); ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>