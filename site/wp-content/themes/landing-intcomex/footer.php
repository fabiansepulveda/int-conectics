    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 box-left">
                    <p>Para conocer más de CONECT-ICS visítanos en <strong><a href="http://www.conect-ics.com/" target="_blank">www.conect-ics.com</a></strong></p>
                </div>
                <div class="col-xs-12 col-sm-6 box-right">
                    <p>© Conect Ics 2016 - Todos los derechos reservados  -  Términos legales</p>
                </div>
            </div>
        </div>
    </footer>
</div><!-- #general-wrapper -->
<?php wp_footer(); ?>
</body>
</html>